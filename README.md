**Instalacja systemu Arch Linux**

Pełna instalacja systemu operacyjnego wraz z zalecanymi ustawieniami znajduje się w odpowiedniej [dokumentacji](https://wiki.archlinux.org/index.php/installation_guide). W celu poprawy bezpieczeństwa należy także skorzystać z [porad](https://wiki.archlinux.org/index.php/Security) zmniejszających prawdopodobieństwo włamania się do systemu.

---

## Pobranie i instalacja repozytorium

Zakładając, że zaintalowałeś już system oraz odpowiednio skonfigurowałeś sieć, przyszedł czas na sklonowanie tego repozytorium.

1. Zainstaluj pakiet **git** za pomocą komendy `pacman -S git`.
2. Wykonaj komendę `git clone https://bitbucket.org/bla-ke/~` oraz zaloguj się do BitBucket.
3. Skopiuj odpowiednie pliki, używając polecenia `cp ~/master/* ~/`.

---

## Katalogi

### .fonts

Zawiera wszystkie darmowe/bezpłatne do użytku prywatnego czcionki, które zostają załadowane do systemu. W celu zmniejszenia rozmiaru folderu można usunąć dowolne z nich.


### .config/GIMP

Przystosowany do wersji 2.10 - zawiera przyspieszające pracę wtyczki programu (gimpscripter) oraz patterny różnorakiej maści.


### .config/gtk-3.0

W środku katalogu znajduje się plik `settings.ini` odpowiadający za wyświetlanie stylu GTK w wersji 3. W celu poprawnej interpretacji kodu, należy zainstalować następujące pakiety: `arc-gtk-theme`, `deepin-icon-theme`, `elementary-icon-theme` (AUR). 


### .config/i3

Odpowiada za prawidłowe wyświetlanie stylu i3 - menedżera okien. Można go dowolnie konfigurować. Domyślny klawisz akcji to "Alt". Zintegrowany z MPD, umożliwia kontrolę muzyki przez klawisze funkcyjne.


### .config/mpd

Zawiera pliki konfiguracyjne MPD - daemona muzyki. Domyślny katalog znajduje się pod `~/Muzyka`. Nie należy zapominać o nadaniu odpowiednich praw w celu odczytu i zapisu utworów.


### .config/openbox

W środku zostały umieszczone pliki - `menu.xml`, `rc.xml` i `autostart` - odpowiedzialne za prawidłowe wyświetlanie stylu Openbox - menedżera okien. Można go dowolnie konfigurować.